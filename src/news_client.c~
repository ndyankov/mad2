
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <uuid/uuid.h>
#include <packedobjectsd/packedobjectsd.h>

/* global variables */
#define XML_DATA "database.xml"
#define XML_SCHEMA "news.xsd"
static char client_id[36];

/* function prototypes */
void send_response(packedobjectsdObject *pod_obj, char *client_id, char *article_title, double price, char *sender_id);
int prepare_response(packedobjectsdObject *pod_obj, char *sender_id, char *article_title, double max_price);
int process_search(packedobjectsdObject *pod_obj, xmlDocPtr search, char *xpathExpr);

/* function definitions */
void send_response(packedobjectsdObject *pod_obj, char *client_id, char *article_title, double price, char *sender_id)
{
  /* Declare variables */
  char price_string[50];
  xmlDocPtr doc_response = NULL;
  xmlNodePtr news_node = NULL, message_node = NULL, response_node = NULL;
  
  LIBXML_TEST_VERSION;

  ///////////////////// Creating Root and other parent nodes ///////////////////

  doc_response = xmlNewDoc(BAD_CAST "1.0");

  /* create pod node as root node */
  news_node = xmlNewNode(NULL, BAD_CAST "news");
  xmlDocSetRootElement(doc_response, news_node);

  message_node = xmlNewChild(news_node, NULL, BAD_CAST "message", BAD_CAST NULL);
  response_node = xmlNewChild(message_node, NULL, BAD_CAST "response", BAD_CAST NULL);
    
  ///////////////////// Creating child elements inside response node ///////////////////
  
  /* create child elements to hold data */
  sprintf(price_string,"%g", price);
  xmlNewChild(response_node, NULL, BAD_CAST "sender-id", BAD_CAST sender_id);
  xmlNewChild(response_node, NULL, BAD_CAST "client-id", BAD_CAST client_id);
  xmlNewChild(response_node, NULL, BAD_CAST "article-title", BAD_CAST article_title);
  xmlNewChild(response_node, NULL, BAD_CAST "price", BAD_CAST price_string);

  ///////////////////// Sending response to the searcher ///////////////////

  /* send the response doc to the searcher */
  if(packedobjectsd_send(pod_obj, doc_response) == -1){
    printf("message could not be sent\n");
    exit(EXIT_FAILURE);
  }
  printf("response sent to the searcher...\n");
  //xml_dump_doc(doc_response);

  xmlFreeDoc(doc_response);
}

int prepare_response(packedobjectsdObject *pod_obj, char *sender_id, char *article_title, double max_price)
{
  /* Declare variables */
  int i;
  int size;
  double price = 0.0;
  char *title = NULL;
  char xpath_exp[1000];
  xmlDocPtr doc_database = NULL;

  printf("received a search request...\nchecking in news database...\n");
  ///////////////////// Initialising XML document ///////////////////

  if((doc_database = xml_new_doc(XML_DATA)) == NULL) {
    printf("did not find database.xml file");
    exit(EXIT_FAILURE);
  }
  
  xmlXPathContextPtr xpathp = NULL;
  xmlXPathObjectPtr result = NULL;

  ///////////////////// Initialising XPATH ///////////////////

  /* setup xpath context */
  xpathp = xmlXPathNewContext(doc_database);
  if (xpathp == NULL) {
    printf("Error in xmlXPathNewContext.");
    xmlXPathFreeContext(xpathp);
    return -1;
  }

  if(xmlXPathRegisterNs(xpathp, (const xmlChar *)NSPREFIX, (const xmlChar *)NSURL) != 0) {
    printf("Error: unable to register NS.");
    xmlXPathFreeContext(xpathp);
    return -1;
  }

  ///////////////////// Evaluating XPATH expression ///////////////////
  
  sprintf(xpath_exp, "/news/message/database/movie[title='%s']/price", article_title);
    
  /* Evaluate xpath expression */
  result = xmlXPathEvalExpression((const xmlChar *)xpath_exp, xpathp);
  if (result == NULL) {
    printf("Error in xmlXPathEvalExpression.");
    xmlXPathFreeObject(result); 
    xmlXPathFreeContext(xpathp);
    return -1;
  }

  ///////////////////// Processing result ///////////////////

  /* check if xml doc consists of data with title matching value from article_title variable" */
  if(xmlXPathNodeSetIsEmpty(result->nodesetval)) {
    xmlXPathFreeObject(result); 
    xmlXPathFreeContext(xpathp);
    printf("the article does not exist on the database\n");
    return -1;
  }
  else {
    size = result->nodesetval->nodeNr;
    xmlNodePtr cur = result->nodesetval->nodeTab[0];

    for(i = 0; i < size; i++) 
      {
	if(!(xmlStrcmp(cur->name, (const xmlChar *)"price")))
	  {
	    xmlChar *key;
	    key = xmlNodeListGetString(doc_database, cur->xmlChildrenNode, 1);
	    price = atof((char *)key);
	    xmlFree(key);	  
	  }

	cur = cur->next;
      }

    ///////////////////// Comparing search data with news database ///////////////////

    /* compare max price from search with price from database */
 
    if(price <= max_price) {
      printf("the article exists on the database and matches price limit\n");
      
      ///////////////////// Sending  search response ///////////////////

      /* send response to searcher */
      send_response(pod_obj, client_id, article_title, price, sender_id);
    }
    else {
      printf("the movie exists on the database but does not match price limit\n");
    }
  }
 
  ///////////////////// Freeing ///////////////////

  free(title);
  xmlFreeDoc(doc_database);
  return 1;
}

int process_search(packedobjectsdObject *pod_obj, xmlDocPtr doc_search, char *xpathExpr)
{
  /* Declare variables */
  double max_price = 0.0;
  char *sender_id = NULL;
  char *article_title = NULL;
  xmlXPathContextPtr xpathCtxPtr = NULL;
  xmlXPathObjectPtr xpathObjPtr = NULL;

  ///////////////////// Initialising XPATH ///////////////////

  /* setup xpath context */
  xpathCtxPtr = xmlXPathNewContext(doc_search);
  if (xpathCtxPtr == NULL) {
    printf("Error in xmlXPathNewContext.");
    xmlXPathFreeContext(xpathCtxPtr);
    return -1;
  }

  if(xmlXPathRegisterNs(xpathCtxPtr, (const xmlChar *)NSPREFIX, (const xmlChar *)NSURL) != 0) {
    printf("Error: unable to register NS.");
    xmlXPathFreeContext(xpathCtxPtr);
    return -1;
  }

  ///////////////////// Evaluating XPATH expression ///////////////////

  /* Evaluate xpath expression */
  xpathObjPtr = xmlXPathEvalExpression((const xmlChar *)xpathExpr, xpathCtxPtr);
  if (xpathObjPtr == NULL) {
    printf("Error in xmlXPathEvalExpression.");
    xmlXPathFreeObject(xpathObjPtr); 
    xmlXPathFreeContext(xpathCtxPtr);
    return -1;
  }

  /* check if xml doc matches "/news/message/search" */
  if(xmlXPathNodeSetIsEmpty(xpathObjPtr->nodesetval)) {
    xmlXPathFreeObject(xpathObjPtr); 
    xmlXPathFreeContext(xpathCtxPtr);
    return -1;
  }

  ///////////////////// Processing search broadcast ///////////////////

  /* the xml doc matches "/news/message/search" */
  xmlNodePtr cur = xmlDocGetRootElement(doc_search);
  while(cur != NULL)
    {
      if(!(xmlStrcmp(cur->name, (const xmlChar *)"sender-id")))
	{
	  while(cur != NULL)
	    {
	      if(!(xmlStrcmp(cur->name, (const xmlChar *)"sender-id")))
		{
		  xmlChar *key;
		  key = xmlNodeListGetString(doc_search, cur->xmlChildrenNode, 1);
		  sender_id = strdup((char *)key);
		  xmlFree(key);	  
		}

	      if(!(xmlStrcmp(cur->name, (const xmlChar *)"article-title"))) {
		xmlChar *key;
		key = xmlNodeListGetString(doc_search, cur->xmlChildrenNode, 1);
		article_title = strdup((char *)key);
		xmlFree(key);
	      }

	      if(!(xmlStrcmp(cur->name, (const xmlChar *)"max-price"))) {
		xmlChar *key;
		key = xmlNodeListGetString(doc_search, cur->xmlChildrenNode, 1);
		max_price = atof((char *)key);
		xmlFree(key);
	      }
	     
	      cur = cur->next;   /* traverse to the next XML element */
	    }
	  printf("\n            ***** search request details ******\n");
	  printf("              sender id: %s \n", sender_id);
	  printf("              article title: %s \n", article_title);
	  printf("              max price: %g\n\n", max_price);

	  ///////////////////// Checking on database ///////////////////

	  /* checking if search broadcast matches record on the database */
	  prepare_response(pod_obj, sender_id, article_title, max_price);  
	  break; /* exit the while loop */
	}
      
      cur = cur->xmlChildrenNode;  /* traverse to next XML node */
    }

  ///////////////////// Freeing ///////////////////

  free(sender_id);
  free(article_title);
  xmlXPathFreeObject(xpathObjPtr); 
  xmlXPathFreeContext(xpathCtxPtr);
  
  return 1;
}

/* main function */
int main(int argc, char *argv [])
{ 
  /* Declare variables */
  uuid_t buffer;
  xmlDocPtr doc_search = NULL;
  packedobjectsdObject *pod_obj = NULL;

  ///////////////////// Initialising ///////////////////

  /* Initialise packedobjectsd */
  if((pod_obj = init_packedobjectsd(XML_SCHEMA)) == NULL) {
    printf("failed to initialise libpackedobjectsd\n");
    exit(EXIT_FAILURE);
  }

  ///////////////////// Generating unique id ///////////////////

  uuid_generate_random(buffer);
  uuid_unparse(buffer, client_id);
  printf("client's unique user id: %s\n", client_id);

  ///////////////////// Receiving search broadcast ///////////////////

  while(1)
    {
      /* waiting for search broadcast */
      printf("waiting for search broadcast\n");
      if((doc_search = packedobjectsd_receive(pod_obj)) == NULL) {
	printf("message could not be received\n");
	exit(EXIT_FAILURE);
      }
 
      usleep(100000); /* Allow searcher program some time to prepare receiving */
      // xml_dump_doc(doc_search);

      ///////////////////// Processing search broadcast ///////////////////

      /* process search broadcast to retrieve search details */
      process_search(pod_obj, doc_search, "/news/message/search");
      xmlFreeDoc(doc_search);
    }

  ///////////////////// Freeing ///////////////////

  /* free memory created by packedobjectsd but we should never reach here! */
  free_packedobjectsd(pod_obj);

  return EXIT_SUCCESS;
}
