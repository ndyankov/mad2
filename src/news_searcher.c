
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <uuid/uuid.h>
#include <pthread.h>
#include <packedobjectsd/packedobjectsd.h>

/* global variables */
#define XML_SCHEMA "news.xsd"
static char user_id[36];

/* function prototypes */
int read_response(xmlDocPtr doc_response, char *xpathExpr);
xmlDocPtr create_search(packedobjectsdObject *pod_obj, char *user_id, char *article_title, char *max_price);
void *receive_response(void *pod_obj);
void *send_search(void *pod_obj);

/* function definitions */
xmlDocPtr create_search(packedobjectsdObject *pod_obj, char *user_id, char *article_title, char *max_price)
{
  /* Declare variables */
  xmlDocPtr doc_search = NULL;
  xmlNodePtr news_node = NULL, message_node = NULL, search_node = NULL;
  
  LIBXML_TEST_VERSION;

  ///////////////////// Creating Root and other parent nodes ///////////////////

  doc_search = xmlNewDoc(BAD_CAST "1.0");

  /* create pod node as root node */
  news_node = xmlNewNode(NULL, BAD_CAST "news");
  xmlDocSetRootElement(doc_search, news_node);

  message_node = xmlNewChild(news_node, NULL, BAD_CAST "message", BAD_CAST NULL);
  search_node = xmlNewChild(message_node, NULL, BAD_CAST "search", BAD_CAST NULL);
    
  ///////////////////// Creating child elements inside response node ///////////////////
  
  /* create child elements to hold data */
  xmlNewChild(search_node, NULL, BAD_CAST "sender-id", BAD_CAST user_id);
  xmlNewChild(search_node, NULL, BAD_CAST "article-title", BAD_CAST article_title);
  xmlNewChild(search_node, NULL, BAD_CAST "max-price", BAD_CAST max_price);

  // xmlFreeDoc(doc_search);
  return doc_search;
}

int read_response(xmlDocPtr doc_response, char *xpathExpr)
{
  /* Declare variables */
  char *sender_id = NULL;
  xmlXPathContextPtr xpathCtxPtr = NULL;
  xmlXPathObjectPtr xpathObjPtr = NULL;

  ///////////////////// Initialising XPATH ///////////////////

  /* setup xpath context */
  xpathCtxPtr = xmlXPathNewContext(doc_response);
  if (xpathCtxPtr == NULL) {
    printf("Error in xmlXPathNewContext.");
    xmlXPathFreeContext(xpathCtxPtr);
    return -1;
  }

  if(xmlXPathRegisterNs(xpathCtxPtr, (const xmlChar *)NSPREFIX, (const xmlChar *)NSURL) != 0) {
    printf("Error: unable to register NS.");
    xmlXPathFreeContext(xpathCtxPtr);
    return -1;
  }

  ///////////////////// Evaluating XPATH expression ///////////////////

  /* evaluate xpath expression */
  xpathObjPtr = xmlXPathEvalExpression((const xmlChar*)xpathExpr, xpathCtxPtr);
  if (xpathObjPtr == NULL) {
    printf("Error in xmlXPathEvalExpression.");
    xmlXPathFreeObject(xpathObjPtr); 
    xmlXPathFreeContext(xpathCtxPtr);
    return -1;
  }

  /* check if  xml doc matches "/news/message/response" */
  if(xmlXPathNodeSetIsEmpty(xpathObjPtr->nodesetval)) {
    xmlXPathFreeObject(xpathObjPtr); 
    xmlXPathFreeContext(xpathCtxPtr);
    return -1;
  }

  ///////////////////// Processing XML document ///////////////////

  /* the xml doc matches "/news/message/response" */
  xmlNodePtr cur = xmlDocGetRootElement(doc_response);
  while(cur != NULL)
    {
      if(!(xmlStrcmp(cur->name, (const xmlChar *)"sender-id")))
	{
	  while(cur != NULL) 
	    {
	      if(!(xmlStrcmp(cur->name, (const xmlChar *)"sender-id")))
		{
		  xmlChar *key;
		  key = xmlNodeListGetString(doc_response, cur->xmlChildrenNode, 1);
		  sender_id = strdup((char *)key);
		  xmlFree(key);	  
		}
	      cur = cur->next; /* traverse to the next XML element */
	    }
	  
	  if((strcmp(sender_id, user_id) == 0)) {
	    free(sender_id);
	    return 1;
	  }
	  break; /* exit while loop */
	}
      cur = cur->xmlChildrenNode; /* traverse to next xml node */
    }

  ///////////////////// Freeing ///////////////////

  xmlXPathFreeObject(xpathObjPtr); 
  xmlXPathFreeContext(xpathCtxPtr);
  
  return -1;
}

void *receive_response(void *pod_obj)
{
  int ret;
  xmlDocPtr doc_response = NULL;
  packedobjectsdObject *pod_object =  (packedobjectsdObject *) pod_obj;
 
  ///////////////////// Receiving search response ///////////////////
  while(1)
    {
      if((doc_response = packedobjectsd_receive(pod_object)) == NULL) {
	printf("message could not be received\n");
	exit(EXIT_FAILURE);
      }
      
      /* ignore if sender-id doesn't match its own id */
      ret = read_response(doc_response, "/news/message/response");
      if(ret == 1) {
      	printf("search response received...\n");
      	xml_dump_doc(doc_response);
      }
      xmlFreeDoc(doc_response);
      usleep(1000);
    }
}

void *send_search(void *pod_obj)
{
  int quit = 0;
  char quit_str[10];
  char article_title[500];
  char max_price[50];
  xmlDocPtr doc_search = NULL;
  packedobjectsdObject *pod_object =  (packedobjectsdObject *) pod_obj;
  
  ///////////////////// Creating search request ///////////////////
  while(quit != 3) 
    {
      printf("Enter 1 to create new search request\n");
      printf("Enter 2 to send search request\n");
      printf("Enter 3 to quit the program\n");
      quit = atoi(gets(quit_str));

      switch(quit) 
	{
	case 1:
	  printf("Please enter your search details below\n");
	  printf("Please enter the news title\n");
	  if(gets(article_title) == NULL) {
	    printf("Title input unsuccessful\n");
	  }
	  printf("Please enter the maximum price\n");
	  if(gets(max_price) == NULL) {
	    printf("Price input unsuccessful\n");
	  }	    

	  if(doc_search != NULL) {
	    xmlFreeDoc(doc_search); // free xml doc pointer if used
	  }
	  doc_search = create_search(pod_object, user_id, article_title, max_price);
	  //xml_dump_doc(doc_search);
	  break;
	case 2:
	  /* send the search doc to the clients */
	  if(doc_search == NULL) {
	    printf("Please create a search request using option 1 first\n");
	    break;
	  }
	  ///////////////////// Sending search request broadcast ///////////////////
	  if(packedobjectsd_send(pod_object, doc_search) == -1){
	    printf("message could not be sent\n");
	    exit(EXIT_FAILURE);
	  }
	  printf("search request sent to the clients...\n");
	  //xml_dump_doc(doc_search);
	  break;
	case 3:
	  printf("This program is now quitting...\n");
	  exit(EXIT_SUCCESS);
	  break;
	default:
	  printf("Unknown option\n");
	  break;
	}
    }
  return pod_object;
}

/* main function */
int main(int argc, char *argv [])
{ 
  /* Declare variables */
  uuid_t buffer;
  pthread_t thread_receiver;
  pthread_t thread_searcher;
  packedobjectsdObject *pod_obj = NULL;
  
  ///////////////////// Initialising packedobjectsd ///////////////////

  /* Initialise packedobjectsd */
  if((pod_obj = init_packedobjectsd(XML_SCHEMA)) == NULL) {
    printf("failed to initialise libpackedobjectsd\n");
    exit(EXIT_FAILURE);
  }

  ///////////////////// Generating unique id ///////////////////

  uuid_generate_random(buffer);
  uuid_unparse(buffer, user_id);
  printf("searcher's unique user id: %s\n", user_id);


  ///////////////////// Initialising threads ///////////////////

  /* initialise thread to execute send_search() function */
  if (pthread_create( &thread_searcher, NULL, send_search, (void *) pod_obj)) {
    fprintf(stderr, "Error creating searcher thread \n");
    exit(EXIT_FAILURE);
  }

  /* initialise thread to execute receive_response() function */
  if (pthread_create( &thread_receiver, NULL, receive_response, (void *) pod_obj)) {
    fprintf(stderr, "Error creating receiver thread \n");
    exit(EXIT_FAILURE);
  }

  ///////////////////// Sending search request ///////////////////

  /* Join the thread to send search request */
  if(pthread_join( thread_searcher, NULL)) {
    fprintf(stderr, "Error joining searcher thread \n");
    exit(EXIT_FAILURE);
  }
   
  ///////////////////// Receiving search response ///////////////////

  /* Join the thread to receive search response */
  if(pthread_join( thread_receiver, NULL)) {
    fprintf(stderr, "Error joining searcher thread \n");
    exit(EXIT_FAILURE);
  }
  
  ///////////////////// Freeing ///////////////////
 
  free_packedobjectsd(pod_obj);
  return EXIT_SUCCESS;
}
