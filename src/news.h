
typedef struct {
  unsigned id;
  int active;
  char *name;
} News;

News *make_news(unsigned id);
void free_news(News *news);
void make_news_active(News *news);
void set_news_name(News *news, char *name);
int is_news_active(News *news);
int serialize_news(char *buffer, News *news);
int deserialize_news(char *buffer, News *news);
void print_news(News *news);
News *alloc_blank_news();
