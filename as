commit 33e3363173ced9f583b60388a729689f2e56c291
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 13:27:39 2013 +0000

    Adding code to news_client.c

commit 3d3f215258c5e33656a8a2d8da253c0d131ec447
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 13:19:29 2013 +0000

    successfully execute all the three news_files

commit 352656b2b88cd84e5aa9ff6498a0c2b9abecfda6
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 13:13:33 2013 +0000

    Adding database.xml to the git

commit be79d88593003dff4c3dc47870a31d3bd4a46029
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 13:08:13 2013 +0000

    Adding search xml to the git

commit dad1445a912e704d2c0514e7573fc82d32163af8
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 13:03:43 2013 +0000

    Add the news.xml to git

commit 9f9146c3fde25c43a3512414ed7136c42d742bda
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 13:03:22 2013 +0000

    Add the news.xsd to the git

commit 5ee9a57d206d20d76c0c45f6712ef426833eadc0
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 12:58:55 2013 +0000

    Adding code to src/Makefile.am

commit 4340b75247b8908da875d594151aac5eb37a72f4
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 12:57:23 2013 +0000

    Adding code to news_server.c.c

commit 53621195a737c2bc5971f99feccddd4a0eaef48b
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 12:56:19 2013 +0000

    Adding code to news_searcher.c

commit 0814eeb5ba1ed4fd12df5cfaf73942fe248c1bdd
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 12:55:22 2013 +0000

    Adding code to news_receiver.c

commit 135a264c7b68db91910ed99bb5e7d05196862a82
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 12:44:55 2013 +0000

    Amending configure.ac and src/main.c

commit ea8dafe39a2bda3ce1586147b244e8e0e53669a4
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 12:40:12 2013 +0000

    Successfully execute the hellonews

commit 04ef8a6f7b0c42bab786f59f38be3790d8aa30a0
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 12:36:17 2013 +0000

    src/Makefile.am

commit a37cb7eef7b921040be2c7d1e8534d4b49acd274
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 12:34:42 2013 +0000

    news.c

commit aa7e50864d9bb2fbe8f12d448811bcbf77bf5bc0
Author: Guest <guest-rUViG1@etc0356u101246.student.tvu.ac.uk>
Date:   Fri Jan 25 12:33:11 2013 +0000

    Amending configure.ac

commit 24f4e493ba0f28434ca885ff83b889dc62aa0e31
Author: Nikolay Dyankov <niki@nikolay.dlink.com>
Date:   Sun Nov 25 11:14:30 2012 +0000

    Server c file was created and main.c was amended in order to run simple server task

commit 822c3ea198d1c2554b993eac374b582780e41331
Author: Nikolay Dyankov <niki@nikolay.dlink.com>
Date:   Sun Nov 25 10:43:54 2012 +0000

    Creating server.c file

commit 28ce63d323097acae278a9f74a08c58fd70707f1
Author: Nikolay Dyankov <niki@nikolay.dlink.com>
Date:   Sun Nov 25 10:37:35 2012 +0000

    Ammend main.c , news.h and news.c in order to implement serialising and deserialising of news1

commit b36decfa14391eb0c9ca24e5aa432c1c9d5d77c5
Author: Nikolay Dyankov <niki@nikolay.dlink.com>
Date:   Sun Nov 25 10:08:54 2012 +0000

    Create separate news.h header file

commit 152fd244f470b8c97e9adce81e9bd4d47111e0bc
Author: Nikolay Dyankov <niki@nikolay.dlink.com>
Date:   Sun Nov 25 10:06:45 2012 +0000

    Create separate news.c file

commit b36f02c3b4e9d0adb1f2d38f05170f193fcf7eeb
Author: Nikolay Dyankov <niki@nikolay.dlink.com>
Date:   Sun Nov 25 10:01:37 2012 +0000

    Amending main.c file to add string structure to both entries news1 and news2

commit f7a9892a8b5c16fa23e89eaf58b262b7ef194fb9
Author: niki <niki@nikolay.dlink.com>
Date:   Sun Nov 25 09:50:22 2012 +0000

    Ammend the main.c file and add another news

commit a8cdbb9e3d75097f09db286ce4c646157c9be5a6
Author: niki <niki@nikolay.dlink.com>
Date:   Sun Nov 25 09:42:10 2012 +0000

    Amending the main.c file to add simple structure
